package JavaPractise.Employee;

// import java.util.*;
import java.io.*;


public class Employee {

    String name;
    int age;
    String designation;
    double salary;

    public static void main(String args[]) {
        
        Employee bob  = new Employee("Bob Bobber");
        Employee mary = new Employee("Mary Anne");
        
        bob.empAge(26);
        bob.empDesignation("Senior Software Engineer");
        bob.empSalary(1000);
        bob.printEmployee();
    }

  

    public Employee (String name){
        this.name = name;
    }

    public void empAge(int empAge){
        age = empAge;
    }

    public void empDesignation(String empDesig){
        designation = empDesig;
    }

    public void empSalary(double empSalary){
        salary = empSalary;
    }

    public void printEmployee(){
        System.out.println("Name: " + name);
        System.out.println("Age: " + age);
        System.out.println("Designation: " + designation);
        System.out.println("salary: " + salary);
    }

}




