package JavaPractise.Employee;

// import java.util.*;
import java.io.*;

class EmployeeTest {

    public static void main(String args[]) {
        
        Employee bob = new Employee("Bob Bobber");
        
        bob.empAge(26);
        bob.empDesignation("Senior Software Engineer");
        bob.empSalary(1000);
        bob.printEmployee();
    }
} 