import java.lang.reflect.Array;
import java.util.ArrayList; // import the ArrayList class

public class EnigmaMachine {

    private Reflector reflector;
    private ArrayList<Rotor> rotors;
    private PlugBoard plugBoard = new PlugBoard();

    public EnigmaMachine(ArrayList<Rotor> rotors, Reflector reflector) {
        this.rotors = rotors;
        this.reflector = reflector;
        System.out.println("machine initiated");
    }

    public String decryptMessage(String message) {
//        do jazz....
        return "decrypted message is: " + message;
    }

    public String encryptMessage(String message) {
//        do jazz .....
        return "encrypted message is: " + message;
    }

    public ArrayList getRotors() {
        return this.rotors;
    }

    public Reflector getReflector() {
        return reflector;
    }
}
