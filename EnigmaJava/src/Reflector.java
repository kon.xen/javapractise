import java.util.Dictionary;
import java.util.Hashtable;

public class Reflector {
    private String name;
    private Dictionary<Integer, Integer> pins = new Hashtable<>();

    private String message = "";

    public Reflector(String name, String pins) {
        this.name = name;
        for (int i = 1; i < 27; i++) {
            this.pins.put(i, pins.indexOf(i));
        }
        System.out.println("Reflector "+ this.name + " is initiated ");
    }

    public String getMessage() {
        return this.message;
    }
}
