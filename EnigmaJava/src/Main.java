import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList; // import the ArrayList class
import java.lang.reflect.Array;
import java.util.Dictionary;
import java.util.*;

public class Main {
    public static void main(String[] args) {

        System.out.println(" ");
        System.out.println("*********************************************************");
        System.out.println("** This is the enigma Machine simulation programme :0) **");
        System.out.println("******************* Crafted by KX :0) *******************");
        System.out.println("*********************************************************");
        System.out.println(" ");

//    here call all the other jazz
        //Load what you need....
        FileLoader fileLoader = new FileLoader();

        //  load the rotor data
        JSONArray rotorData = fileLoader.loadJson("src/rotors.json");
        Dictionary<String, JSONObject> rotorList = new Hashtable<>();
        for (Object r : rotorData) {
            JSONObject rotor = (JSONObject) r;
            rotorList.put(rotor.get("name").toString(), rotor);
        }

        //  load the reflector data
        JSONArray reflectorData = fileLoader.loadJson("src/reflectors.json");
        Dictionary<String, JSONObject> reflectorList = new Hashtable<>();
        for (Object r : reflectorData) {
            JSONObject reflector = (JSONObject) r;
            reflectorList.put(reflector.get("name").toString(), reflector);
        }

        //  load the machine settings 😄
        JSONArray settings = fileLoader.loadJson("src/machineSettings.json");
        JSONObject machineSettings = (JSONObject) settings.get(0);
        System.out.println(machineSettings);

        // "rotors"=rotor settings [0=name,1=start] "reflector" = name
        JSONArray rotorSettings = (JSONArray) machineSettings.get("rotors");
        String reflectorName = machineSettings.get("reflector").toString();

        //Make rotors to pass to the machine.
        ArrayList<Rotor> rotors = new ArrayList<>(rotorSettings.size());
        for (int i = 0; i < rotorSettings.size(); i++) {
            JSONArray current = (JSONArray) rotorSettings.get(i);
            String name = current.get(0).toString();
            String startPos = current.get(1).toString();
            String pins = rotorList.get(name).get("wiring").toString();
            String notch = rotorList.get(name).get("notch").toString();
            String turnover = rotorList.get(name).get("turnover").toString();

            rotors.add(new Rotor(name, startPos, notch, turnover, pins));
        }

        //Make a reflector to pass to the machine.
        Reflector reflector = new Reflector(
        // TODO: 06/04/2023 make use of the starting position to set the rotors
                reflectorName,
                reflectorList.get(reflectorName).get("wiring").toString()
        );

        //  Make a machine
        EnigmaMachine machine = new EnigmaMachine(rotors, reflector);

        for (Object r : machine.getRotors()) {
            Rotor rotor = (Rotor) r;
            System.out.println("rotor " + ((Rotor) r).getName());
            String thePins =  rotor.getPins().toString();
            System.out.println(thePins);
//            Dictionary thePins = rotor.getPins();
//            for (int x = 0; x < thePins.size(); x++) {
//                System.out.println(thePins.get(x));
//            }
        }
//        System.out.println(machine.getRotors().toString());
//        System.out.println(machine.getReflector());

        // TODO: 06/04/2023  Set up the  machine

        // TODO: 06/04/2023  Get the encrypted message

        // TODO: 06/04/2023    Decrypt / Encrypt the message
        System.out.println(machine.decryptMessage("this is an encrypted message"));
        System.out.println(machine.encryptMessage("this is a non encrypted message"));
        // TODO: 06/04/2023      Save the decrypted message
        // TODO: 06/04/2023     Display the decrypted message
    }
}