
// Java program to read JSON from a file

import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.*;

public class FileLoader {
    private String message = "File Loader initiated";


    public FileLoader() {
        System.out.println(this.getMessage());
    }

    public String getMessage() {
        return this.message;
    }

    public JSONArray loadJson( String filePath ) {

        JSONArray data;
        JSONParser parser = new JSONParser();

        try {
            data = (JSONArray) parser.parse( new FileReader( filePath ) );

        } catch (IOException | ParseException e) {
            throw new RuntimeException(e);
        }

        return data;
    }
}
