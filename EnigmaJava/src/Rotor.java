
import java.util.Dictionary;
import java.util.Hashtable;

public class Rotor {
    private String name;
    private String notch;
    private String turnover;
    private Dictionary pins = new Hashtable<>();
    private Integer start;

    public Rotor(String name, String start, String notch, String turnover, String pins) {
        this.name = name;
        this.notch = notch;
        this.turnover = turnover;
        this.start = Integer.valueOf(start);
        int key = 0;
        int pin = this.start;

        while (key < 26) {

            if (pin >= 26) {
                pin = 0;
            }
            this.pins.put(key, pins.charAt(pin));
            key += 1;
            pin += 1;
        }
//        for (int i = this.start ; i < 26; i++) {
////            System.out.println(pins.charAt(i));
//            this.pins.put(i, pins.charAt(i));
//        }
        System.out.println("rotor: " + this.name + " is initiated");
    }

    public String getNotch() {
        return this.notch;
    }

    public void setNotch(String notch) {
        this.notch = notch;
    }

    public String getTurnover() {
        return this.turnover;
    }

    public void setTurnover(String turnover) {
        this.turnover = turnover;
    }

    public Integer getStart() {
        return this.start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public String getName() {

        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Dictionary getPins() {
        return this.pins;
    }

    public void setPins(Dictionary pins) {
        this.pins = pins;
    }
}
