package JavaPractise.Trees;

// example of tree Data object and insert, accessing , printing methods

import java.util.*;


public class Node {   

  Node left;
  Node right;
  int data;

  public static void main(String[] args) { 
       
      Node tree = new Node(56);        
        tree.insert(20);
        tree.insert(10);
        tree.insert(5);
        tree.insert(8);
        tree.printInOrder();
    } 

  public Node(int data){
    this.data = data;
  }

  public void insert (int value){

    if (value <= data){
      if (left == null) {
        left = new Node(value);
      } else {
        left.insert(value);
      } 
    } else {
        if (right == null) {
          right = new Node(value);
        } else {
          right.insert(value);
        }
    }
    
  }

  public boolean contains(int value) {
    if ( value == data ) {
      return true;
    } else if  ( value < data ) {
      if (left == null) {
        return false;
      } else {
        return left.contains(value);
      }
    } else {
      if (right == null ){
        return false;
      } else {
        return right.contains(value);
      }
    }

  }
  

  public void printInOrder() {
    if (left != null) {
      left.printInOrder();
    }
    System.out.println(data);
    if (right != null) {
      right.printInOrder();
    }
  }
 
}

  