import java.util.function.Function;

public class BraceChecker {

    public boolean isValid(String braces) {
    
        int the_count = 0;

        for (int i = 0; i < braces.length(); i++) {

            if ( braces.indexOf(i) == '(' || braces.indexOf(i) == '{' || braces.indexOf(i) == '['){
                the_count++;
            } else {
                the_count--;
                }                
        }        
       
        if (the_count == 0){
            return true;
        }

        return false;            
    }
        
}