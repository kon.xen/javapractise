package com.example.demo.Repository;

import com.example.demo.Data.ToDo;
import org.springframework.data.mongodb.repository.MongoRepository;
public interface ToDoRepository extends MongoRepository<ToDo,String>{

}
