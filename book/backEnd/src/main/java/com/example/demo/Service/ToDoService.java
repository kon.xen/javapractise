package com.example.demo.Service;

import com.example.demo.Data.ToDo;
import com.example.demo.Exception.EntityNotFoundException;
import com.example.demo.Repository.ToDoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ToDoService {
    @Autowired
    private ToDoRepository ToDoRepository;

    public List<ToDo> findAll() {
        return findAll();
    }

    public ToDo findById(String id) {
        return ToDoRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    public ToDo save(ToDo toDo) {
        return ToDoRepository.save(toDo);
    }

    public void deleteById(String id) {
        ToDoRepository.deleteById(id);
    }
}

